package client

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder
import shared.ChatMessage

/**
 * Decodes a message sent from the server
 */
class ClientChatReceiver : ByteToMessageDecoder() {


    override fun decode(ctx: ChannelHandlerContext, msg: ByteBuf, out: MutableList<Any>) {
        out.add(ChatMessage.decode(msg))
    }


}