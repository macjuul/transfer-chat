package client

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder
import shared.SetupResponse

/**
 * Decodes the initial response received by the server
 */
class ClientSetupReceiver : ByteToMessageDecoder() {


    override fun decode(ctx: ChannelHandlerContext, msg: ByteBuf, out: MutableList<Any>) {
        val username = SetupResponse.decode(msg).finalUsername


    }


}