package client

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder
import shared.ChatMessage

/**
 * Prepares a chat message to be sent to the server
 */
class ClientChatEmitter : MessageToByteEncoder<ChatMessage>() {


    override fun encode(ctx: ChannelHandlerContext, msg: ChatMessage, out: ByteBuf) {
        msg.encode(out)
    }


}