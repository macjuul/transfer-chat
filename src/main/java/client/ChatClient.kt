package client

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioSocketChannel
import shared.decodeAddress

/* Application entry point */
fun main(args: Array<String>) {
    if(args.isEmpty()) {
        throw IllegalArgumentException("Empty args")
    }

    ChatClient(args) {
        println("Client shutdown: $it")
    }
}

/**
 * The client responsible for connecting to the server
 */
class ChatClient(args: Array<String>, onShutdown: (String) -> Unit) : ChannelInitializer<NioSocketChannel>() {

    private val eventGroup: NioEventLoopGroup
    private val username: String
    private val onShutdown = onShutdown

    lateinit var channel: NioSocketChannel
        private set

    init {
        println("Welcome to Transfer Chat! How do you want to be known as?")
        username = pollUsername()

        println("Connecting to upstream as $username...")

        /* Setup the client bootstrap */
        eventGroup = NioEventLoopGroup()

        val addr = decodeAddress(args[0])
        val bootstrap = Bootstrap()
            .group(eventGroup)
            .channel(NioSocketChannel::class.java)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .option(ChannelOption.TCP_NODELAY, true)
            .handler(this)

        /* Wait for the connection to be established */
        try {
            channel = bootstrap.connect(addr).sync() as NioSocketChannel
        } catch(ex: Throwable) {
            shutdown("Connection refusedl")
        }
    }

    /**
     * Setup the socket channel pipeline
     */
    override fun initChannel(channel: NioSocketChannel) {
        val pipeline = channel.pipeline()

        pipeline.addLast(ClientChatEmitter())
        pipeline.addLast(ClientSetupReceiver())
    }

    /**
     * Shutdown the client
     */
    fun shutdown(message: String) {
        eventGroup.shutdownGracefully().sync()
        if(::channel.isInitialized) channel.closeFuture().sync()
        onShutdown(message)
    }

    companion object {

        /**
         * Request a valid username
         */
        fun pollUsername() : String {
            val username = readLine()!!

            if(username.length < 3 || username.length > 16) {
                println("Username must be between 3 and 16 characters! Try again")
                return pollUsername()
            }

            return username
        }

    }
}