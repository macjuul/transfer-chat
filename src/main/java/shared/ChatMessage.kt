package shared

import io.netty.buffer.ByteBuf
import java.nio.charset.StandardCharsets

/**
 * Represents a chat message sent from one of the clients
 */
class ChatMessage(val user: String, val timestamp: Long, val message: String) : CharSequence, Sendable {

    override val length: Int
        get() = message.length

    /* Sendable implementation */

    override fun encode(buffer: ByteBuf) {
        /* Write the timestamp */
        buffer.writeLong(timestamp)

        /* Write the username length + username */
        val nameBytes = user.toByteArray()
        buffer.writeShort(nameBytes.size)
        buffer.writeBytes(nameBytes)

        /* Write the message length + message */
        val msgBytes = message.toByteArray()
        buffer.writeShort(msgBytes.size)
        buffer.writeBytes(msgBytes)
    }

    companion object {

        fun decode(buffer: ByteBuf) : ChatMessage {
            val timestamp = buffer.readLong()

            val nameLength = buffer.readShort().toInt()
            val username = buffer.readBytes(nameLength).toString(StandardCharsets.UTF_8)

            val msgLength = buffer.readShort().toInt()
            val msg = buffer.readBytes(msgLength).toString(StandardCharsets.UTF_8)

            return ChatMessage(username, timestamp, msg);
        }

    }


    /* CharSequence implementation */

    override fun get(index: Int) = message[index]

    override fun subSequence(startIndex: Int, endIndex: Int) = message.subSequence(startIndex, endIndex)

}