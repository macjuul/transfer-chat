package shared

import io.netty.buffer.ByteBuf
import java.nio.charset.StandardCharsets

/**
 * The response sent by the server to indicate a successful connection
 */
class SetupResponse(val finalUsername: String) : Sendable {

    /* Sendable implementation */

    override fun encode(buffer: ByteBuf) {
        val nameBytes = finalUsername.toByteArray()
        buffer.writeShort(nameBytes.size)
        buffer.writeBytes(nameBytes)
    }

    companion object {

        fun decode(buffer: ByteBuf) : SetupResponse {
            val nameLength = buffer.readShort().toInt()
            val username = buffer.readBytes(nameLength).toString(StandardCharsets.UTF_8)

            return SetupResponse(username)
        }

    }

}