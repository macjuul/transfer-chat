package shared

import java.net.InetSocketAddress

/*
 * Decodes a user supplied address into an InetSocketAddress
 */
fun decodeAddress(address: String) : InetSocketAddress {
    return address.split(':').run { InetSocketAddress(this[0], this[1].toInt()) }
}