package shared

import io.netty.buffer.ByteBuf

/**
 * Indicates that an instance is sendable as network packet
 */
interface Sendable {

    /**
     * Encodes the instance and writes it onto the given ByteBuf
     */
    fun encode(buffer: ByteBuf)

}